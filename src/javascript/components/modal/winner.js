import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinner(fighter) {
  const imageElement = createFighterImage(fighter);
  const modalElement = {
    title: `${fighter.name} win`,
    bodyElement: imageElement
  };
  
  showModal(modalElement);  
}
